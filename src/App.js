import React from 'react';
import { render } from 'react-dom';
import { makeData, Logo, Tips } from './Utils';
import _ from 'lodash';
import { DropdownButton, MenuItem, ButtonToolbar } from 'react-bootstrap';
// Import React Table
import ReactTable from 'react-table';
import moment from 'moment';
import 'react-table/react-table.css';

// pull in the HOC
import treeTableHOC from 'react-table/lib/hoc/treeTable';

// wrap ReacTable in it
// the HOC provides the configuration for the TreeTable
const TreeTable = treeTableHOC(ReactTable);

function getTdProps(state, ri, ci) {
  console.log({ state, ri, ci });
  return {};
}

// getTdProps={getTdProps}
// Expander={Expander}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      dates: Array.apply(null, new Array(7)).map((val, i) =>
        moment()
          .subtract(i, 'days')
          .format('L')
      ),
      sort: 'email'
    };
    this.dataload = this.dataLoad.bind(this);
    this.updateDate = this.updateDate.bind(this);
    this.updateSort = this.updateSort.bind(this);

    var _url = '';
  }

  componentDidMount() {
    const url = `https://s3-us-west-2.amazonaws.com/files.prolaera.com/errors/kinesis/msGraphStream-prod/msGraphStream-prod_${
      this.state.dates[0]
    }.json`;
    console.log(url);
    this.dataload(url).then(data => {
      this.setState({ data });
    });
  }

  dataLoad(url) {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      // let url = "https://files.prolaera.com/errors/kinesis/msGraphStream-prod/msGraphStream-prod_08_13_2018.json";
      xhr.open('GET', url, true);
      xhr.responseType = 'json';
      xhr.onload = function() {
        if (xhr.status === 200) {
          var newData = xhr.response;
          var reformattedData = newData.map(obj => {
            return {
              ...obj.body.attendee,
              error: obj.error,
              created_at: obj.created_at,
              function: obj.body.function
            };
          });
          resolve(reformattedData);
        } else {
          resolve([]);
        }
      };
      xhr.send();
    });
  }

  updateDate(event) {
    console.log(event.target.value, 'DATE UPDATE');
    event.preventDefault();
    const url = `https://s3-us-west-2.amazonaws.com/files.prolaera.com/errors/kinesis/msGraphStream-prod/msGraphStream-prod_${
      event.target.value
    }.json`;
    console.log(url);
    this.dataload(url).then(data => {
      this.setState({ data });
    });
  }

  updateSort(event) {
    console.log(event.target.value, 'Sort update');
    event.preventDefault();
    this.setState({ sort: event.target.value });
  }
  render() {
    const { data, dates, sort } = this.state;

    const divStyle = {
      display: 'flex',
      'justify-content': 'space-around'
    };
    // now use the new TreeTable component
    return (
      <div>
        <form style={divStyle}>
          <label>
            Select Date:
            <select value={this.state.date} onChange={this.updateDate}>
              {dates.map((date, index) => (
                <option key={index} value={date.replace(/\//g, '_')}>
                  {date}
                </option>
              ))}
            </select>
          </label>
          <label>
            Sort by:
            <select value={this.state.sort} onChange={this.updateSort}>
              <option key="1" value="email">
                Email
              </option>
              <option key="2" value="event_id">
                Event ID
              </option>
            </select>
          </label>
        </form>
        <TreeTable
          filterable
          defaultFilterMethod={(filter, row, column) => {
            const id = filter.pivotId || filter.id;
            return row[id] !== undefined
              ? String(row[id])
                  .toLowerCase()
                  .includes(filter.value.toLowerCase())
              : true;
          }}
          data={data}
          pivotBy={['company_id', sort]}
          columns={
            [
              // we only require the accessor so TreeTable
              // can handle the pivot automatically
              { accessor: 'company_id' },
              { accessor: sort },
              { Header: 'Type', accessor: 'function' },
              { Header: 'Error', accessor: 'error' }
            ] // any other columns we want to display
          }
          defaultPageSize={10}
          SubComponent={row => {
            // a SubComponent just for the final detail
            const columns = [
              {
                Header: 'Property',
                accessor: 'property',
                width: 200,
                Cell: ci => {
                  return `${ci.value}:`;
                },
                style: {
                  backgroundColor: '#DDD',
                  textAlign: 'right',
                  fontWeight: 'bold'
                }
              },
              { Header: 'Value', accessor: 'value' }
            ];
            const rowData = Object.keys(row.original).map(key => {
              return { property: key, value: row.original[key].toString() };
            });
            return (
              <div style={{ padding: '10px' }}>
                <ReactTable
                  data={rowData}
                  columns={columns}
                  pageSize={rowData.length}
                  showPagination={false}
                />
              </div>
            );
          }}
        />
        <br />
        <Tips />
        <Logo />
      </div>
    );
  }
}

export default App;
